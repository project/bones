function bones_toggle_menu_option() {

  selected = $("input[@name=option]:checked").val();
  
  if (selected == 'createnew') { //Hide the existing menu pulldown
    $('#bones-pid').hide('slow');
    $('#bones-newmenu').show('slow');  
  }
  
  if (selected == 'existing') { //Hide the new menu textfield
    $('#bones-pid').show('slow');
    $('#bones-newmenu').hide('slow');    
  }
  
  if (selected == 'none') { //Hide both
    $('#bones-pid').hide();
    $('#bones-newmenu').hide();         
  }
  
}